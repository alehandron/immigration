$(document).ready(function(){
    $('.slider').slick({
        dots: false,
        arrows:true,
        fade: true,
        speed: 800,
        cssEase: 'linear',
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: $('.btn--prev'),
        nextArrow: $('.btn--next')
      });

});
