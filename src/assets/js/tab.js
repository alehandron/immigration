
function tab(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tab-content");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.opacity = "0";
        tabcontent[i].style.left = "100%";
    }
    tablinks = document.getElementsByClassName("btn");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace("btn--active", " ");
    }
    document.getElementById(tabName).style.opacity = "1";
    document.getElementById(tabName).style.left = "6rem";
    evt.currentTarget.className += "btn--active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();




document.addEventListener('DOMContentLoaded', () => {
	// First real dialog starts around 1:47
    function dashInit(){
        const source = ['../images/reviews-02.mp4, ../images/reviews-01.mp4, ../images/reviews-03.mp4 '];
        for (let i = 0; i < source.length; i++) {
            source[i]
        }
        dash.initialize(video,  source[i], true);

    }
	// For more dash options, see https://github.com/Dash-Industry-Forum/dash.js
	const dash = dashjs.MediaPlayer().create();
	const video = document.getElementsByClassName('video');
    // dash.initialize(video, dashInit(), true);





	// Update caption tracks after initializing Plyr to get the generated captions
	// For more options see: https://github.com/sampotts/plyr/#options
    const players = Array.from(document.querySelectorAll('video')).map(p => new Plyr(p));


	// Expose player and dash so they can be used from the console
	window.players = players;
	window.dash = dash;
});
